package com.example.androidsample3.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androidsample3.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        String username = extras.getString("username");
        String password = extras.getString("password");

        TextView welcomeTextView = findViewById(R.id.welcome_text_view);
        String message = welcomeTextView.getText().toString() + " " + username + " " + password;

        welcomeTextView.setText(message);
    }
}
